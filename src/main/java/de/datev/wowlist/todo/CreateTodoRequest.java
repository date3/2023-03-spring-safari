package de.datev.wowlist.todo;

import de.datev.wowlist.note.Note;
import de.datev.wowlist.subtask.Subtask;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;
import java.util.List;

@Getter
@Setter
public class CreateTodoRequest {

    String description;

    String category;

    Instant dueDate;

    // we use the subtask entity directly.
    // it simplifies the mapping, but this technique is discouraged because
    // it binds the domain model directly to the client
    List<Subtask> subtasks;

    List<Note> notes;

}
