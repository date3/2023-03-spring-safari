package de.datev.wowlist.todo;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class CreateTodoResponse {
    private UUID id;
    private String description;
    private boolean done;

    public CreateTodoResponse(Todo todo) {
        this.id = todo.getId();
        this.description = todo.getDescription();
        this.done = todo.isDone();
    }

    public CreateTodoResponse(){}

}
