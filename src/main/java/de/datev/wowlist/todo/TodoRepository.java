package de.datev.wowlist.todo;

import org.springframework.data.jpa.repository.JpaRepository;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

public interface TodoRepository extends JpaRepository<Todo, UUID> {

    List<Todo> findByDescription(String description);

    List<Todo> findByCategoryAndDueDateBefore(String category, Instant dueDate);
}
