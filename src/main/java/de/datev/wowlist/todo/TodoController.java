package de.datev.wowlist.todo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

import static java.util.stream.Collectors.toList;

@RestController
public class TodoController {

    private TodoService todoService;

    @Autowired
    public TodoController(TodoService todoService) {
        this.todoService = todoService;
    }

    @GetMapping("/todos")
    public List<TodoResponse> getAllTodos(){
        List<Todo> allTodos = todoService.getAllTodos();

        return allTodos
                .stream()
                .map(TodoResponse::new)
                .collect(toList());
    }

    @GetMapping("/todos/{id}")
    public TodoResponse getTodoById(@PathVariable("id") UUID id){
        Todo todoById = todoService.getTodoById(id);
        return new TodoResponse(todoById);
    }

    @GetMapping("/todos/due")
    public List<TodoResponse> getAllDueTodos(@RequestParam("dueDate") Instant dueDate, @RequestParam("category") String category){
        List<Todo> todos = todoService.getTodosByDueDateAndCategory(dueDate, category);

        return todos
                .stream()
                .map(TodoResponse::new)
                .collect(toList());
    }

    @GetMapping("/todos/query")
    public List<TodoResponse> getTodosByDescription(@RequestParam("description") String description){
        List<Todo> todos = todoService.getTodoByDescription(description);

        return todos
                .stream()
                .map(TodoResponse::new)
                .collect(toList());
    }

    @PostMapping("/todos")
    @ResponseStatus(HttpStatus.CREATED)
    public CreateTodoResponse createTodo(@RequestBody CreateTodoRequest createTodoRequest){
        Todo todo = todoService.createTodo(createTodoRequest.getDescription(),
                createTodoRequest.getCategory(),
                createTodoRequest.getDueDate(),
                createTodoRequest.getSubtasks(),
                createTodoRequest.getNotes());

        return new CreateTodoResponse(todo);
    }

    @PutMapping("/todos/{id}")
    public void changeTodo(@PathVariable("id") UUID id, @RequestBody ChangeTodoRequest changedTodo){
        Todo todoToChange = new Todo(changedTodo.getDescription());
        todoService.changeTodo(id, todoToChange);
    }

}
