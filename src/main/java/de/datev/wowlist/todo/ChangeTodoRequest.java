package de.datev.wowlist.todo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChangeTodoRequest {
    private String description;

}
