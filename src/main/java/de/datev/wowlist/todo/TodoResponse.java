package de.datev.wowlist.todo;

import de.datev.wowlist.subtask.SubTaskResponse;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

import static java.util.stream.Collectors.toList;

@Getter
@Setter
public class TodoResponse {

    private String category;
    private Instant dueDate;

    private List<SubTaskResponse> subtasks;

    private List<NoteResponse> notes;

    public TodoResponse(Todo todo){
        this.id = todo.getId();
        this.description = todo.getDescription();
        this.done = todo.isDone();
        this.category = todo.getCategory();
        this.dueDate = todo.getDueDate();
        this.subtasks = todo.getSubtasks().stream().map(SubTaskResponse::new).collect(toList());
        this.notes = todo.getNotes().stream().map(NoteResponse::new).collect(toList());
    }

    public TodoResponse(){}

    private UUID id;
    private String description;

    private boolean done;

}
