package de.datev.wowlist.todo;

import de.datev.wowlist.note.Note;
import de.datev.wowlist.subtask.Subtask;
import org.springframework.http.HttpStatusCode;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Service
public class TodoService {

    private final TodoRepository todoRepository;

    public TodoService(TodoRepository todoRepository){
        this.todoRepository = todoRepository;
    }

    public List<Todo> getAllTodos() {
        return todoRepository.findAll();
    }

    public Todo getTodoById(UUID id) {
        return todoRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatusCode.valueOf(404)));
    }

    public Todo createTodo(String description, String category, Instant dueDate, List<Subtask> subtasks, List<Note> notes) {
        var todo = new Todo(description, category, dueDate, subtasks, notes);

        // the returned savedTodo object differs in a way that i.e. generated uuid´s are set
        Todo savedTodo = todoRepository.save(todo);
        return savedTodo;
    }

    public void changeTodo(UUID id, Todo changedTodo) {
        Todo todoToChange = todoRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatusCode.valueOf(404)));

        todoToChange.setDescription(changedTodo.getDescription());

        todoRepository.save(todoToChange);
    }

    public void clearTodos() {
        todoRepository.deleteAll();
    }

    public List<Todo> getTodoByDescription(String description) {
        return todoRepository
                .findByDescription(description);
    }

    public List<Todo> getTodosByDueDateAndCategory(Instant dueDate, String category) {
        return todoRepository.findByCategoryAndDueDateBefore(category, dueDate);
    }
}
