package de.datev.wowlist.todo;

import de.datev.wowlist.note.Note;
import de.datev.wowlist.subtask.Subtask;
import jakarta.persistence.*;
import jakarta.validation.constraints.Future;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Entity
public class Todo {
    @Id
    private UUID id;

    @Column
    @NotEmpty
    @Size(max = 500)
    private String description;

    @Column
    @Size(max = 100)
    private String category;

    @Future
    @Temporal(TemporalType.TIMESTAMP)
    private Instant dueDate;

    @Column
    private boolean done;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "todo")
    private List<Subtask> subtasks = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "todo")
    private List<Note> notes = new ArrayList<>();


    public Todo(String description) {
        this.id = UUID.randomUUID();
        this.description = description;
    }

    private Todo() {
    }

    public Todo(String description, String category, Instant dueDate) {
        this.id = UUID.randomUUID();

        this.description = description;
        this.category = category;
        this.dueDate = dueDate;
    }

    public Todo(String description, String category, Instant dueDate, List<Subtask> subtasks, List<Note> notes) {
        this.id = UUID.randomUUID();
        this.description = description;
        this.category = category;
        this.dueDate = dueDate;
        this.subtasks = subtasks;
        this.notes = notes;

        if(this.subtasks != null){
            for(Subtask subtask: subtasks) {
                subtask.setTodo(this);
            }
        }

        if(this.notes != null){
            for(Note note: notes) {
                note.setTodo(this);
            }
        }

    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public UUID getId() {
        return id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Instant getDueDate() {
        return dueDate;
    }

    public void setDueDate(Instant dueDate) {
        this.dueDate = dueDate;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public List<Subtask> getSubtasks() {
        return subtasks;
    }

    public void setSubtasks(List<Subtask> subtasks) {
        this.subtasks = subtasks;
    }

    public List<Note> getNotes() {
        return notes;
    }

    public void setNotes(List<Note> notes) {
        this.notes = notes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Todo todo = (Todo) o;
        return id.equals(todo.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}