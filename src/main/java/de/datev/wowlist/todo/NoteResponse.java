package de.datev.wowlist.todo;

import de.datev.wowlist.note.Note;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NoteResponse {

    private String description;

    public NoteResponse(Note note){
        this.description = note.getDescription();
    }
}
