package de.datev.wowlist;

import jakarta.validation.ConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class TodoExceptionHandler extends ResponseEntityExceptionHandler {

        @ResponseBody
        @ExceptionHandler(ConstraintViolationException.class)
        public ResponseEntity<?> handleControllerException(ConstraintViolationException ex) {
            ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.BAD_REQUEST, ex.getMessage());
            return new ResponseEntity<>(problemDetail, HttpStatus.BAD_REQUEST);
        }

}
