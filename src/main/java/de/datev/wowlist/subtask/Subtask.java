package de.datev.wowlist.subtask;

import de.datev.wowlist.todo.Todo;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;

import java.util.Objects;
import java.util.UUID;

@Entity
public class Subtask {

    @Id
    // generate the primary key
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @Column
    @NotEmpty
    @Size(max = 500)
    private String description;

    // ToDo(1) -> Subtask(n)

    @ManyToOne
    @JoinColumn(name = "todo_id")
    private Todo todo;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Todo getTodo() {
        return todo;
    }

    public void setTodo(Todo todo) {
        this.todo = todo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Subtask subtask = (Subtask) o;
        return id.equals(subtask.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
