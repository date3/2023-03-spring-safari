package de.datev.wowlist.subtask;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SubTaskResponse {

    private String description;

    public SubTaskResponse(Subtask s) {
        this.description = s.getDescription();
    }

    private SubTaskResponse() {}

}
