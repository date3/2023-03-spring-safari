package de.datev.wowlist;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(AdditionalConfiguration.class)
public class AppConfiguration {

}
