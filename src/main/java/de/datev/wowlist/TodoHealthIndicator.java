package de.datev.wowlist;

import de.datev.wowlist.todo.TodoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component
public class TodoHealthIndicator implements HealthIndicator {

    @Autowired
    private TodoRepository todoRepository;

    @Override
    public Health health() {
        long todoCount = todoRepository.count();
        if (todoCount == 0) {
            return Health.down().build();
        }
        if (todoCount > 10) {
            return Health.status("OUT_OF_SERVICE").build();
        }

        return Health.up().build();
    }
}
