package de.datev.wowlist;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties
public class AdditionalConfiguration {

    private String environment;

    private boolean testMode, sendMail;

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public boolean isTestMode() {
        return testMode;
    }

    public void setTestMode(boolean testMode) {
        this.testMode = testMode;
    }

    public boolean isSendMail() {
        return sendMail;
    }

    public void setSendMail(boolean sendMail) {
        this.sendMail = sendMail;
    }
}
