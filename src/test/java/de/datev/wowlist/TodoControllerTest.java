package de.datev.wowlist;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.datev.wowlist.todo.CreateTodoRequest;
import de.datev.wowlist.todo.CreateTodoResponse;
import de.datev.wowlist.todo.TodoService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class TodoControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    TodoService todoService;

    @BeforeEach
    public void clearDatabase() {
        todoService.clearTodos();
    }

    @Test
    public void noTodos_getAllTodos_emptyList() throws Exception {
        mockMvc.perform(get("/todos"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize(0)))
                .andReturn();
    }

    @Test
    public void createTodo_todoCreated_oneTodoReturned() throws Exception {
        var createTodoRequest = new CreateTodoRequest();
        createTodoRequest.setDueDate(Instant.now().plus(10, ChronoUnit.DAYS));
        createTodoRequest.setCategory("important");
        createTodoRequest.setDescription("Kaffee kaufen");

        String requestAsString = objectMapper.writeValueAsString(createTodoRequest);

        mockMvc.perform(post("/todos")
                        .content(requestAsString)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isCreated());

        mockMvc.perform(get("/todos"))
                .andExpect(jsonPath("$", Matchers.hasSize(1)))
                .andExpect(jsonPath("$[0].dueDate").isNotEmpty())
                .andExpect(jsonPath("$[0].category").isNotEmpty())
                .andExpect(jsonPath("$[0].description").isNotEmpty());
    }

    @Test
    public void getTodoById_idIsExisting_oneTodoReturned() throws Exception {
        String contentAsString = mockMvc.perform(post("/todos")
                        .content("{\"description}\": \"test\"}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn().getResponse().getContentAsString();

        CreateTodoResponse response = objectMapper.readValue(contentAsString, CreateTodoResponse.class);

        mockMvc.perform(get("/todos/{id}", response.getId()))
                .andExpect(status().isOk());
    }

    @Test
    public void getTodoByDescription_oneTodoWithDescriptionExists_oneTodoReturned() throws Exception {
        String contentAsString = mockMvc.perform(post("/todos")
                        .content("{\"description\": \"test\"}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn().getResponse().getContentAsString();

        CreateTodoResponse response = objectMapper.readValue(contentAsString, CreateTodoResponse.class);

        mockMvc.perform(get("/todos/query")
                        .queryParam("description", response.getDescription()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.hasSize(1)));
    }

    @Test
    public void changeTodo_exists_todoIsChanged() throws Exception {
        // create one
        String response = mockMvc.perform(post("/todos")
                        .content("{\"description\": \"test\"}")
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString();

        CreateTodoResponse createdTodo = objectMapper.readValue(response, CreateTodoResponse.class);

        mockMvc.perform(put("/todos/" + createdTodo.getId())
                        .content("{ \"description\": \"Kaffee kochen\"}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        mockMvc.perform(get("/todos/{id}", createdTodo.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.description").value("Kaffee kochen"));
    }
}
