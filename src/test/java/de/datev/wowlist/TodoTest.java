package de.datev.wowlist;

import de.datev.wowlist.todo.Todo;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Set;

public class TodoTest {

    @Test
    public void description_emptyIsEmpty_isInvalid() {
        Todo todo = new Todo("",
                "wichtig",
                Instant.now().plus(1, ChronoUnit.DAYS)
        );

        // ======
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<Todo>> violations = validator.validate(todo);
        // ======

        Assertions.assertEquals(1, violations.size());
    }

}
