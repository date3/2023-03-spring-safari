package de.datev.wowlist;

import de.datev.wowlist.todo.Todo;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.UUID;

public class HashcodeEqualsTest {

    @Test
    @Disabled
    public void koan(){
        String name1 = new String("m");
        String name2 = new String("m");

        Assertions.assertTrue(name1 == name2);
    }

    /**
     * This tests demonstrates why it is important to have a proper hashcode/equals implementation on
     * all entities.
     */
    @Test
    public void regression(){
        ArrayList<Todo> todos = new ArrayList<>();

        Todo newTodo = new Todo("test");
        UUID uuid = UUID.randomUUID();
        newTodo.setId(uuid);

        todos.add(newTodo);

        // todo is saved in db

        // time goes by... and a new object with the same properties is created
        // this could i.e. happen if a new HTTP call comes in
        Todo todoToDelete = new Todo("test");
        todoToDelete.setId(uuid);

        // are we able to delete the todo?
        Assertions.assertTrue(todos.remove(todoToDelete));
        Assertions.assertTrue(todos.isEmpty());
    }

}
