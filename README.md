
# Behandelte Themen am 29.03.

## Custom Query Methoden
Mit Spring Data ist es möglich, eigene Query Methoden zu schreiben, um Daten aus der Datenbank zu selektieren.

Ein Beispiel befindet sich in `TodoRepository` mit der Methode

````java    
List<Todo> findByCategoryAndDueDateBefore(String category, Instant dueDate);
````

Diese Methode selektiert alle Todos mit der `category` und dem `dueDate` _vor_ angegebenen `dueDate`.

Dokumentation:

* https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#repositories.query-methods.query-creation
* https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#repository-query-keywords

## Automatische Generierung des Primary Keys
Der Primary Key kann auch automatisch mit `@GeneratedValue` generiert werden. Dann muss er nicht mehr manuell
gesetzt werden. (siehe `Todo` Entity)

## Relationen
Mit JPA können Beziehungen zwischen Entitäten beschrieben werden. Das sind:

* @OneToOne
* @OneToMany
* @ManyToOne
* @ManyToMany

Für unser `Todo` Beispiel haben wir dafür die `Subtasks` an der `Todo` Entity eingeführt.

````java
@OneToMany(cascade = CascadeType.ALL, mappedBy = "todo")
private List<Subtask> subtasks;
````

### Unidirektionale Beziehungen
Ist die `@OneToMany` Annotation nur auf einer Seite vorhanden - also am `Todo` und nicht am `Subtask` sprechen wir von 
einer unidirektionalen Beziehung.

### Bidirektionale Beziehungen
Ist auf beiden Seiten eine Annotation für die Relation vorhanden (also `@OneToMany` auf den `Todo` und `@ManyToOne` am `Subtask`)
sprechen wir von bidirektional Beziehungen.

# Wesentliche Codeänderungen
Wesentliche Codeänderungen zum Nachlesen sind in:
* Todo.java: Dort sind die Subtasks dazugekommen
* Subtask.java: Das sind die Subtasks, die eine many-to-one Beziehung mit den Todos haben
* CreateTodoRequest.java: Hier ist der Subtask dazugekommen, über die REST Api können nun Subtasks angelegt werden
* TodoController.java: In der `createTodo` Methode werden die Subtasks weitergereicht
* TodoRepository.java: Hier gibt es einen custom query Methode, um alle Todos anhand von `category` und `dueDate` zu selektieren

# Behandelte Themen am 30.03.

## Bidirektionale Beziehungen
Mit den JPA Annotations können Beziehung zwischen Entities hergestellt werden. Diese können uni-, oder bidirektional sein. Bei bidirektionalen Beziehungen muss
darauf geachtet werden, das beide Seiten der Relation verknüpft werden.

## Hashcode und Equals
Bei Entities sollte die hashcode/equals Methoden implementiert werden, damit Hibernate die Datensätze korrekt identifieren kann.

## Typische Struktur eines Spring Projektes
Empfohlen ist die Strukturierung nach Feature:

https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#using.structuring-your-code

## Validierungen mit Jakarta Validations
Mit Jakarta Validation können einfach Validierungen vorgenommen werden. Die Validierung erfolgt wahlweise früh auf der Controllerebene

````java
@PostMapping("/todos")
@ResponseStatus(HttpStatus.CREATED)
public Todo createTodo(@Valid @RequestBody CreateTodoRequest createTodoRequest){
    Todo todo = mapRequestToTodo(createTodoRequest);

    return todoService.createTodo(todo);
}
````

oder an den `Entities` direkt. 

Beides hat seine Vor-, und Nachteile:

* Auf der Controllerebene: Daten werden früh an der Systemgrenze validiert
* An den Entities: Validierung passiert in der Businesslogik und die Entities können Konsitenz sicherstellen

## Exception Handling mit @Controller Advice
Mit der Annotation `@ControllerAdvice` und `@ExceptionHandler` können globale Exception Handler implementiert werden. Siehe dazu `TodoExceptionHandler.java`

## Spring Actuator
Das Modul spring-boot-actuator hilft dabei, die Spring Anwendung in Produktion zu überwachen. 

Der `/acutator/health` gibt den aktuellen Zustand der Anwendung zurück, das Ergebnis kann über eigene `HealtIndicator` Implementationen individualisiert werden.  

Spring Actuator ermöglichst ebenfalls das Sammeln von eigenen Metriken und den Transfer in die bekannten System (z. B. Grafite)

## Spring Security
Spring Security managed Authentifizierung und Authorisierung der Anwendung über eine eigene `FilterChain`, die sich Abarbeitung des HTTP Requests einhängt.

Die FilterChain kann über eine eigene Konfiguration angepasst werden - siehe `SecurityConfiguration`.
